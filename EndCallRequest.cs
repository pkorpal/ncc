﻿using System;

namespace NCC
{
    class EndCallRequest
    {
        int connectionId;
        string source;
        Directory dir;

        public EndCallRequest(string msg)
        {
            //  0                   1           2       3   
            //  END_CALL_REQUEST    SOURCE      C1      CONNECTION_ID 5
            string[] tmp = msg.Split(' ');
            this.connectionId = Int32.Parse(tmp[3]);
            this.source = tmp[1];
            Console.WriteLine("NCC: Closing connection with ID {0}", this.connectionId);
            dir = new Directory();
        }

        public string processEndCallRequest()
        {
            string device = dir.getEdgeNode(source);
            int port = getDevicePort(device);
            RouterConnection rc = new RouterConnection();
            Console.WriteLine("NCC: Sending to device {0} query to close connection with id {1}", device, this.connectionId);
            if(rc.endCall(port, this.connectionId))
            {
                Console.WriteLine("NCC: Succesfully closed connection with ID {0}", this.connectionId);
                return "CONNECTION CLOSED";
            }
            else
            {
                Console.WriteLine("NCC: Error while closing connection with ID {0}", this.connectionId);
                return "ERROR WHILE CLOSING CONNECTION";
            }
        }

        public int getDevicePort(string device)
        {
            RouterConnection rc = new RouterConnection();
            rc.getConnectedRouters();
            return rc.getDevicePort(device);
        }
    }
}
