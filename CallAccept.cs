﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace NCC
{
    class CallAccept
    {
        string destination;
        string source;
        int port;

        public CallAccept(string destination, string source, int port)
        {
            this.destination = destination;
            this.source = source;
            this.port = port;
        }

        public bool sendCallAccept()
        {
            Socket socket = new Socket(IPAddress.Loopback.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            socket.ReceiveBufferSize = 256;
            socket.SendBufferSize = 256;
            socket.DontFragment = true;
            socket.NoDelay = true;
            socket.SendTimeout = 500;
            socket.LingerState = new LingerOption(true, 2);

            socket.Connect(IPAddress.Loopback, port);

            Byte[] bytes = new Byte[256];
            string msg = "CALL_ACCEPT SOURCE " + this.source;
            Console.WriteLine("NCC: Sending CALL_ACCEPT to client {0}", this.destination);
            bytes = Encoding.UTF8.GetBytes(msg);

            try
            {
                socket.Send(bytes);
                Byte[] response = new Byte[256];
                socket.Receive(response);
                string sresponse = Encoding.ASCII.GetString(response);
                Console.WriteLine("NCC: CALL_ACCEPT RESPONSE {0}", sresponse);
                if (sresponse.Contains("CALL_ACCEPTED"))
                    return true;
                else
                    return false;
            }
            catch (Exception e)
            {
                Console.WriteLine("CallAccept.sendCallAccept: Error while sending CALL_ACCEPT to client");
                Console.WriteLine(e);
                return false;
            }
        }
    }
}