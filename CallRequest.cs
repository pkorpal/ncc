﻿using System;

namespace NCC
{
    class CallRequest
    {
        String source;
        String destination;
        int connectionId;
        int throughput;

        Policy policy;
        Directory directory;

        public CallRequest(string msg, int connectionId)
        {
            policy = new Policy();
            directory = new Directory();
            this.connectionId = connectionId;
            string[] smsg = msg.Split(' ');
            source = smsg[2];
            destination = smsg[4];
            throughput = Int16.Parse(smsg[6]);
            policy.checkForPolicyIssue(source, destination, throughput);
            Console.WriteLine("NCC: Setting up connection between {0} and {1}", this.source, this.destination);
        }

        public string processCallRequest()
        {
            bool connection_set = false;
            bool connection_accepted = false;

            connection_set = this.getPath();

            if (connection_set)
            {
                Console.WriteLine("NCC: Path found and resources reserved. Sending CALL ACCEPT to {0}", this.destination);
                connection_accepted = checkIfCallAccepted();
            }
            else
            {
                Console.WriteLine("NCC: Call cannot be setup");
                return "CONNECTION FAILED";
            }

            if (connection_set && connection_accepted)
            {
                Console.WriteLine("NCC: Connection successfully set");
                return "CONNECTION SET CONNECTION_ID " + this.connectionId;
            }
            else
            {
                Console.WriteLine("NCC: {0} refused to accept call", this.destination);
                return "CONNECTION FAILED"; // 
            }
        }

        public bool checkIfCallAccepted()
        {
            int destination_port;
            RouterConnection rc = new RouterConnection();
            destination_port = rc.getDevicePort(this.destination);
            CallAccept ca = new CallAccept(this.destination, this.source, destination_port);
            return ca.sendCallAccept();
        }

        public int getDevicePort(string device)
        {
            RouterConnection rc = new RouterConnection();
            rc.getConnectedRouters();
            return rc.getDevicePort(device);
        }

        public bool getPath()
        {
            string device = directory.getEdgeNode(source);
            int port = getDevicePort(device);
            RouterConnection rc = new RouterConnection();
            Console.WriteLine("NCC: Sending PATH_QUERY to device {0}", device);
            bool result = rc.sendToRouter(port, source, destination, connectionId, this.throughput);
            return result;
        }
    }
}
