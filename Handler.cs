﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace NCC
{

    class Handler
    {
        Socket socket;
        Byte[] bytes;
        String data;
        int connectionId;

        public Handler(Socket s, int connectionId)
        {
            this.socket = s;
            this.bytes = new Byte[100];
            this.connectionId = connectionId;
            start();
        }

        private void start()
        {
            Thread t = new Thread(new ThreadStart(run));
            t.Start();
        }

        private void run()
        {
            int byteREc = socket.Receive(bytes);
            data = Encoding.ASCII.GetString(bytes, 0, byteREc);
            Console.WriteLine("NCC: Received messeage {0}", data);
            // e.g. CALL_REQUEST SOURCE 10.0.0.0 DESTINATION 10.10.0.0 THROUGHPUT 150
            if (data.IndexOf("CALL_REQUEST") > -1) // NCC receives from client a request to setup a connection with another client
            {
                CallRequest callRequest = new CallRequest(data, connectionId);
                string r = callRequest.processCallRequest();
                byte[] response = Encoding.UTF8.GetBytes(r);
                socket.Send(response);
            }
            // e.g. CONNECT ROUTER R1 PORT 11000
            else if (data.IndexOf("CONNECT") > -1) // NCC receives from a router a HELLO message with its name and listening port
            {
                string[] smsg = data.Split(' ');
                RouterConnection routerConnection = new RouterConnection();
                routerConnection.setRouterDetails(smsg[2], smsg[4]);
                routerConnection.getConnectedRouters();
                routerConnection.updateConnectedRouters();
            }
            else if (data.Contains("END_CALL"))
            {
                EndCallRequest ecr = new EndCallRequest(data);
                string r = ecr.processEndCallRequest();
                byte[] response = Encoding.UTF8.GetBytes(r);
                socket.Send(response);
            }
        }
    }
}